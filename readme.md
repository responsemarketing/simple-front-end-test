# Snowboard Company II

Please fork this repository for this test and submit a pull request when it is completed. Make sure to include any server requirements to run the site locally are included for your submission, this is not complicated simple HTML/CSS and JavaScript should do the trick.

In the psds folder you will find the home page desktop layout for this project.

This layout needs to be developed with a mobile first philosophy.

There is only one design completed for a 1024 minimum width so please use your best judgment on when and where to re-position elements for smaller browsers. If you have any questions please ask.

For mobile screens you can use a “Three Line Menu Navicon” in the upper right and logo upper left. 

#### The navigation can be placed behind one of the following:
1.	Off Canvas Menu
2.	Slide Down Menu
3.	Native Select

### Web Font: 
http://www.google.com/fonts#UsePlace:use/Collection:Open+Sans|Open+Sans+Condensed:300 

The main image is not a slider, it is a single image with call out text. It should also always be full width in the browser window.

The three column text should never break unless they are switched to a one column format for mobile/small screens.

The footer text should fall below the social icons on smaller screens and should never go to one column. The social icons should never break the line.

#### We are supporting down to:
* IE 8
* Safari 5.1.10
* Firefox 18.0.1
* iPhone 4

We are only developing the home page at this time, We would like to get feed back from the client when the home page is completed to see if it meets their expectations.